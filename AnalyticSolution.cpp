// AnalyticSolution.cpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-08-06 22:46:42 (jmiller)>

// This file implements the analytic solution methods.
#include "AnalyticSolution.hpp"
#include "Variables.hpp"

using AnalyticSolution::AMPLITUDE;
using AnalyticSolution::OMEGA;
using AnalyticSolution::K;

double AnalyticSolution::getU(double t, double x) {
  return AMPLITUDE*cos(K*x - OMEGA*t);
}

double AnalyticSolution::getRho(double t, double x) {
  return OMEGA*AMPLITUDE*sin(K*x - OMEGA*t);
}

double AnalyticSolution::normSquaredError(const double t,
					  const Grid& g) {
  double errorSum = 0;
  double error;
  double x;
  for (int i = 0; i < g.get_nx_local(); ++i) {
    x = g.get_x(i);
    error = g.get_var(Variables::U,i) - AnalyticSolution::getU(t,x);
    errorSum += error*error;
  }
  return errorSum;
}

void AnalyticSolution::fill(const double t, Grid& g) {
  double x;
  for (int i = 0 - g.NUM_GHOSTS;
       i < g.get_nx_local() + g.NUM_GHOSTS;
       ++i) {
    x = g.get_x(i);
    g.get_var(Variables::U,i) = AnalyticSolution::getU(t,x);
    g.get_var(Variables::RHO,i) = AnalyticSolution::getRho(t,x);
  }
}
