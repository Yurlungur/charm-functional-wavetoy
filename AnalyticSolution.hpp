// AnalyticSolution.hpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-08-06 22:47:26 (jmiller)>

// This file defines everything required to calculate the analytic
// solution.

#ifndef ANALYTIC_SOLUTION_HPP
#define ANALYTIC_SOLUTION_HPP

#include "Grid.hpp"
#include <cmath>
#include "math.h"

namespace AnalyticSolution {
  const double GLOBAL_XMIN=0;
  const double GLOBAL_XMAX=2*M_PI;
  const double WIDTH = GLOBAL_XMAX - GLOBAL_XMIN;
  const double AMPLITUDE=1;
  const double C2 = 1; // speed of light
  const double NU_BAR = 2; // 1/lambda
  const double K = NU_BAR;
  const double OMEGA = sqrt(C2)*K;
  double getU(double t, double x);
  double getRho(double t, double x);
  double normSquaredError(const double t,
			  const Grid& g);
  void fill(const double t, Grid& g);
}

#endif // ANALYTIC_SOLUTION_HPP
