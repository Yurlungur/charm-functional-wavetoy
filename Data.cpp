// Data.cpp
// Time-stamp: <2015-07-06 01:48:51 (jmiller)>
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

// Data is a simple wrapper for a C++ vector but with simple vector
// arithmetic enabled
// Could have inherited from std::vector I wanted a clean, simple interface.

#include "Data.hpp"
#include <cassert>
#include <limits>
#include <pup_stl.h>

Data::Data() { Data(1); }

Data::Data(int size) :
  Data(size, std::numeric_limits<double>::signaling_NaN())
{}

Data::Data(int size, double fill_value) {
  myData.resize(size);
  for (int i = 0; i < size; ++i) {
    myData[i] = fill_value;
  }
}

void Data::copy_data(const Data& d) {
  myData.resize(d.size());
  for (int i = 0; i < d.size(); ++i) {
    myData[i] = d[i];
  }
}

Data::Data(const Data& d) { copy_data(d); }

Data& Data::operator= (const Data& d) { copy_data(d); return *this; }

Data& Data::operator+= (const Data& d) {
  assert (size() == d.size()
	  && "The data must be the same size.");
  for (int i = 0; i < size(); ++i) {
    myData[i] += d[i];
  }
  return *this;
}

Data& Data::operator+= (const double s) {
  for (int i=0; i<size(); ++i) myData[i] += s;
  return *this;
}

Data& Data::operator*=(const double s) {
  for (int i = 0; i<size(); ++i) {
    myData[i] *= s;
  }
  return *this;
}

Data Data::operator-() const {
  Data out(*this);
  out *= -1.;
  return out;
}

Data& Data::operator-=(const Data& d) {
  assert (size() == d.size()
	  && "The data must be the same size.");
  for (int i = 0; i < size(); ++i) {
    myData[i] -= d[i];
  }
  return *this;
}

Data& Data::operator-=(const double s) {
  for (int i=0;i<size();++i) myData[i]-=s;
  return *this;
}

Data operator+(const Data& d1, const Data& d2) {
  Data out(d1);
  out += d2;
  return out;
}

Data operator+(const Data& d, const double s) {
  Data d2(d.size(),s);
  return d + d2;
}

Data operator+(const double s, const Data& d) {
  return d + s;
}

Data operator*(const double s, const Data& d) {
  Data out(d);
  out *= s;
  return out;
}

Data operator*(const Data& d, const double s) {
  return s*d;
}

Data operator-(const Data& d1, const Data& d2) {
  return d1 + -1.0*d2;
}

Data operator-(const Data& d, const double s) {
  return d + -1.*s;
}

Data operator- (const double s, const Data& d) {
  return d - s;
}

int Data::size() const {
  return myData.size();
}


double& Data::operator[] (const int i) {
  return myData[i];
}

double Data::operator[] (const int i) const {
  return myData[i];
}

void Data::resize(int i) { myData.resize(i); }

Data Data::slice(int i1, int i2) {
  Data out(i2-i1);
  for (int i=0; i < out.size(); ++i) {
    out[i] = myData[i+i1];
  }
  return out;
}

int Data::last_index() const { return size()-1; }

bool operator== (const Data& d1, const Data& d2) {
  if (d1.size() != d2.size()) return false;
  for (int i=0; i < d1.size(); ++i) {
    if (d1[i] != d2[i]) return false;
  }
  return true;
}

bool operator!=(const Data& d1, const Data& d2) {
  return !(d1 == d2);
}

std::ostream& operator<< (std::ostream& os, const Data& d) {
  os << "[";
  for (int i = 0; i < d.last_index(); ++i) {
    os << d[i] << ", ";
  }
  os << d[d.last_index()] << "]";
  return os;
}

void Data::pup(PUP::er& p) {
  p | myData;
}
