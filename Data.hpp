// Data.hpp
// Time-stamp: <2015-07-05 17:17:44 (jmiller)>
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

// Data is a simple wrapper for a C++ vector but with simple vector
// arithmetic enable
// Could have inherited from std::vector but I wanted a clean, simple interface.

#ifndef INCLUDE_DATA_HPP
#define INCLUDE_DATA_HPP

#include <vector>
#include <iostream>
#include <pup.h>

class Data {
public:
  // constructors
  Data();
  Data(const Data& d);
  Data(int size);
  Data(int size, double fill_value);

  // Operators
  Data& operator= (const Data& d);
  Data& operator+= (const Data& d);
  Data& operator+= (const double s);
  Data& operator-= (const Data& d);
  Data& operator-= (const double s);
  Data& operator*= (const double s);

  Data operator- () const;

  friend Data operator+ (const Data& d1, const Data& d2);
  friend Data operator+ (const Data& d, const double s);
  friend Data operator+ (const double s, const Data& d);
  friend Data operator* (const double s, const Data& d);
  friend Data operator* (const Data& d, const double s);
  friend Data operator- (const Data& d1, const Data& d2);
  friend Data operator- (const Data& d, const double s);
  friend Data operator- (const double s, const Data& d);

  friend bool operator== (const Data& d1, const Data& d2);
  friend bool operator!= (const Data& d1, const Data& d2);

  friend std::ostream& operator<< (std::ostream& os, const Data& d);

  // Access
  double& operator[] (const int i);
  double operator[] (const int i) const;

  void resize(int i);

  int size() const;

  // Returns the last index in Data.
  int last_index() const;

  // Slicing
  // Returns a new data object of size
  // i2 - i1
  // that is the subset of this between those indices,
  // i1 inclusive, i2 exclusive
  Data slice(int i1,int i2);

  // Charm++ serialization
  void pup(PUP::er& p);

private:
  std::vector<double> myData;
  void copy_data(const Data& d);
};

#endif // INCLUDE_DATA_HPP
