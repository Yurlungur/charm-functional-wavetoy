// Domain.cpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-08-07 12:40:56 (jmiller)>

// This file implements the domain data structure

#include "Domain.hpp"
#include "AnalyticSolution.hpp"
#include "DoneMsg.hpp"
#include "GhostMsg.hpp"
#include "GridMsg.hpp"
#include "ErrorMsg.hpp"
#include "Variables.hpp"
#include "Sides.hpp"

#include <memory>
#include <cassert>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cmath>

#define DONEROUTINE \
  if (isDone != 0) { \
    DoneMsg* m = new DoneMsg(); \
    CkSendToFuture(doneFuture,m);\
    delete this;\
  }

const bool DEBUGGING = false;

// Base constructor
Domain::Domain()
  : myGrid()
  , myNumElements(0)
{ }

// Initial data constructor.
// nx is the number of grid points per element.
Domain::Domain(int nx, int numElements) {
  myNumElements = numElements;
  int globalNx = nx*numElements;
  double dx = (AnalyticSolution::GLOBAL_XMAX
	       -AnalyticSolution::GLOBAL_XMIN)/globalNx;
  double xmin = AnalyticSolution::GLOBAL_XMIN + nx*dx*thisIndex;
  double xmax = xmin + (nx-1)*dx;
  myGrid = Grid(xmin,xmax,nx);
  AnalyticSolution::fill(0.0,myGrid);
}

int Domain::domainMod(int i) {
  int out = i % myNumElements;
  while (out < 0) {
    out += myNumElements;
  }
  return out;
}

void Domain::getGhost(CkFuture f, Side s) {
  GhostMsg* m = new GhostMsg(s,myGrid);
  CkSendToFuture(f,m);
}

void Domain::getGrid(CkFuture f) {
  int size = myGrid.NUM_VARS*myGrid.get_size();
  GridMsg* m = new (size) GridMsg(myGrid);
  CkSendToFuture(f,m);
}

void Domain::getRhs(CkFuture f) {
  /*
    du/dt = rho
    drho/dt =c^2 d^2 u/dx^2
   */
  // ask for ghosts points
  CkFuture rightFuture = CkCreateFuture();
  CkFuture leftFuture = CkCreateFuture();
  int rightIndex = domainMod(thisIndex+1);
  int leftIndex = domainMod(thisIndex-1);
  if (DEBUGGING) {
    ckout << "starting rhs" << endl;
    ckout << "Asking for ghosts" << endl;
  }
  thisProxy[rightIndex].getGhost(rightFuture,Side::LEFT);
  thisProxy[leftIndex].getGhost(leftFuture,Side::RIGHT);
  // calculate interior
  if (DEBUGGING) {
    ckout << "Calculating interior" << endl;
  }
  Grid out(myGrid.get_xmin(),myGrid.get_xmax(),myGrid.get_nx_local());
  for (int i = 0; i < myGrid.get_nx_local(); ++i) {
    out.get_var(Variables::U,i) = myGrid.get_var(Variables::RHO,i);
  }
  for (int i = Grid::NUM_GHOSTS;
       i < myGrid.get_nx_local() - Grid::NUM_GHOSTS; ++i) {
    out.get_var(Variables::RHO,i) =
      AnalyticSolution::C2*myGrid.get_derivative(Variables::U,i);
  }
  // fill ghost points
  if (DEBUGGING) {
    ckout << "waiting for ghosts" << endl;
  }
  std::unique_ptr<GhostMsg> leftMsg( (GhostMsg*) CkWaitFuture(leftFuture) );
  std::unique_ptr<GhostMsg> rightMsg( (GhostMsg*) CkWaitFuture(rightFuture) );
  leftMsg -> fillGrid(myGrid);
  rightMsg -> fillGrid(myGrid);
  // Calculating. The right-hand-side.
  if (DEBUGGING) {
    ckout << "calculating edges" << endl;
  }
  out.get_var(Variables::RHO,0) =
    AnalyticSolution::C2*myGrid.get_derivative(Variables::U,0);
  out.get_var(Variables::RHO,
 	      myGrid.get_nx_local()-Grid::NUM_GHOSTS)
    = AnalyticSolution::C2*myGrid.get_derivative(Variables::U,
						 myGrid.get_nx_local()
						 -Grid::NUM_GHOSTS);
  // Return the rhs
  GridMsg* m = new (out.NUM_VARS*out.get_size()) GridMsg(out);
  CkSendToFuture(f,m);
  if (DEBUGGING) {
    ckout << "Done" << endl;
  }
}

inline void Domain::declareDone(CkCallback cb) {
  // Charm++ boolean operations are for integers. Wasteful if you ask
  // me. Should use chars or bools.
  int isDone = 1;
  contribute(sizeof(int),&isDone,CkReduction::logical_and,cb);
}

void Domain::linearCombination(std::vector<double> coeffs,
			       std::vector<CProxy_Domain> domains,
			       CkCallback cb) {
  assert ( coeffs.size() == domains.size() );
  std::vector<CkFuture> futures(coeffs.size());

  // ask for grids
  for (int i = 0; i < coeffs.size(); ++i) {
    futures[i] = CkCreateFuture();
    domains[i][thisIndex].getGrid(futures[i]);
  }
  
  // first grid
  std::unique_ptr<GridMsg> myGridMsg;
  myGridMsg.reset( (GridMsg*) CkWaitFuture(futures[0]) );
  myGridMsg -> fillGrid(myGrid);
  myGrid *= coeffs[0];

  // the remaining grids
  Grid tempGrid;
  for (int i = 1; i < coeffs.size(); ++i) {
    myGridMsg.reset( (GridMsg*) CkWaitFuture(futures[i]) );
    myGridMsg -> fillGrid(tempGrid);
    tempGrid *= coeffs[i];
    myGrid += tempGrid;
  }

  // cleanup. Release futures. declare done
  for (int i = 0; i < coeffs.size(); ++i) CkReleaseFuture(futures[i]);
  declareDone(cb);
}

void Domain::copy(CProxy_Domain d, CkCallback cb) {
  CkFuture f = CkCreateFuture();
  d[thisIndex].getGrid(f);
  std::unique_ptr<GridMsg> mGridMsg ( (GridMsg*) CkWaitFuture(f) );
  mGridMsg -> fillGrid(myGrid);
  CkReleaseFuture(f);
  declareDone(cb);
}

void Domain::increment(double c, CProxy_Domain d, CkCallback cb) {
  CkFuture f = CkCreateFuture();
  Grid tempGrid;
  d[thisIndex].getGrid(f);
  std::unique_ptr<GridMsg> mGridMsg ( (GridMsg*) CkWaitFuture(f) );
  mGridMsg -> fillGrid(tempGrid);
  tempGrid *= c;
  myGrid += tempGrid;
  CkReleaseFuture(f);
  declareDone(cb);
}

void Domain::setToRhs(CProxy_Domain d, CkCallback cb) {
  CkFuture f = CkCreateFuture();
  d[thisIndex].getRhs(f);
  std::unique_ptr<GridMsg> mGridMsg( (GridMsg*) CkWaitFuture(f) );
  mGridMsg -> fillGrid(myGrid);
  CkReleaseFuture(f);
  declareDone(cb);
}

void Domain::calculateError(double t, CkCallback cb) {
  double normSquaredError = AnalyticSolution::normSquaredError(t,myGrid);
  contribute(sizeof(double),&normSquaredError,CkReduction::sum_double,cb);
}

void Domain::deleteSelf() {
  delete this;
}

void Domain::pup(PUP::er& p) {
  p | myGrid;
}

AddDomains::AddDomains(CProxy_Domain d,
		       std::vector<double> coeffs,
		       std::vector<CProxy_Domain> domains,
		       CkFuture f) {
  doneFuture = f;
  CkCallback cb(CkReductionTarget(AddDomains,done),thisProxy);
  d.linearCombination(coeffs,domains,cb);
}

void AddDomains::done(int isDone) { DONEROUTINE }

CopyDomain::CopyDomain(CProxy_Domain d1, CProxy_Domain d2, CkFuture f) {
  doneFuture = f;
  CkCallback cb(CkReductionTarget(CopyDomain,done),thisProxy);
  d1.copy(d2,cb);
}

void CopyDomain::done(int isDone) { DONEROUTINE }

IncrementDomain::IncrementDomain(CProxy_Domain d1,
				 double c, CProxy_Domain d2,
				 CkFuture f) {
  doneFuture = f;
  CkCallback cb(CkReductionTarget(IncrementDomain,done),thisProxy);
  d1.increment(c,d2,cb);
}

void IncrementDomain::done(int isDone) { DONEROUTINE }

SetToRhs::SetToRhs(CProxy_Domain d1, CProxy_Domain d2, CkFuture f) {
  if (DEBUGGING)  ckout << "Starting SetToRhs" << endl;
  doneFuture = f;
  CkCallback cb(CkReductionTarget(SetToRhs,done),thisProxy);
  if (DEBUGGING) ckout << "Calling d1" << endl;
  d1.setToRhs(d2,cb);
}

void SetToRhs::done(int isDone) { DONEROUTINE }

Output::Output(int timestep, double t,
	       CProxy_Domain d, int numElements,
	       CkFuture fCopyDone,
	       CkFuture fOutputDone) {
  filename = makeFileName(timestep);
  doOutput(timestep,t,d,numElements,fCopyDone,fOutputDone);
}

Output::Output(int timestep, double t,
	       CProxy_Domain d, int numElements,
	       std::string extraLine,
	       CkFuture fCopyDone,
	       CkFuture fOutputDone) {
  filename = makeFileName(timestep,extraLine);
  doOutput(timestep,t,d,numElements,fCopyDone,fOutputDone);
}

void Output::doOutput(int timestep, double t,
		      CProxy_Domain d, int numElements,
		      CkFuture fCopyDone,
		      CkFuture fOutputDone){
  if (DEBUGGING) ckout << "Starting output" << endl;
  // copy d
  dCopy = CProxy_Domain::ckNew(numElements);
  copyFinished = CkCreateFuture();
  CProxy_CopyDomain::ckNew(dCopy,d,copyFinished);
  
  // prepare some stuff while we wait for the copy to finish
  // futures
  prepareFutures(numElements);
  initiateOutput(timestep,t);
  
  // Ensure copy is finished
  DoneMsg* m = ( (DoneMsg*) CkWaitFuture(copyFinished) );
  // We're done with d now. Tell the world.
  CkSendToFuture(fCopyDone,m);
  
  // start asking for output
  for (int i = 0; i < numElements; ++i) {
    dCopy[i].getGrid(gridFutures[i]);
  }
  
  // print the output
  for (int i = 0; i < numElements; ++i) {
    mGridMsg.reset( (GridMsg*) CkWaitFuture(gridFutures[i]) );
    mGridMsg -> fillGrid(g);
    outputGrid();
  }
  
  // cleanup
  for (int i = 0; i < numElements; ++i) CkReleaseFuture(gridFutures[i]);
  dCopy.deleteSelf();
  outfile.close();
  DoneMsg* m2 = new DoneMsg();
  CkSendToFuture(fOutputDone,m2);
  delete this;
}

std::string Output::makeFileName(int timestep) {
  std::stringstream ss;
  ss.str("");
  ss << filePrefix << timestep << filePostfix;
  return ss.str();
}

std::string Output::makeFileName(int timestep,
				 std::string extraLine) {
  std::stringstream ss;
  ss.str("");
  ss << filePrefix << timestep
     << "_" << extraLine
     << filePostfix;
  return ss.str();
}

void Output::outputFirstLine(int timestep, double t) {
  outfile << timestep << whitespaceCharacter
	  << t << whitespaceCharacter
	  << CkWallTimer() << "\n";
}

void Output::outputGrid() {
  for (int xi = 0; xi < g.get_nx_local(); ++xi) {
    outfile << g.get_x(xi)
	    << whitespaceCharacter << g.get_var(Variables::U,xi)
	    << whitespaceCharacter << g.get_var(Variables::RHO,xi)
	    << std::endl;
  }
}

void Output::prepareFutures(int numElements) {
  gridFutures.resize(numElements);
  for (int i = 0; i < numElements; ++i) {
    gridFutures[i] = CkCreateFuture();
  }
}

void Output::initiateOutput(int timestep, double t) {
  // the file
  outfile.open(filename);
  outfile << header;
  // First line
  outputFirstLine(timestep,t);
}

GetNorm2Error::GetNorm2Error(double t, CProxy_Domain d, CkFuture f) {
  errorFuture = f;
  CkCallback cb(CkReductionTarget(GetNorm2Error,sumSquareErrors),thisProxy);
  d.calculateError(t,cb);
}

void GetNorm2Error::sumSquareErrors(double squareError) {
  ErrorMsg* m = new ErrorMsg(sqrt(squareError));
  CkSendToFuture(errorFuture,m);
  delete this;
}

#undef DONEROUTINE
#include "domain.def.h"
