// Domain.hpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-08-07 12:32:18 (jmiller)>

// This header file domains the domain data structure

#ifndef INCLUDE_DOMAIN_HPP
#define INCLUDE_DOMAIN_HPP

#include "domain.decl.h"
#include "Grid.hpp"
#include "Sides.hpp"

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <pup.h>

class Domain : public CBase_Domain {
public:
  Domain(); // Default constructor
  // charm++ migration constructor
  Domain(CkMigrateMessage *msg) {}
  // Initial grid constructor nx is number of grid points per element,
  // numElements is the number of elements
  Domain(int nx, int numElements);
  // Ghost zones
  void getGhost(CkFuture f, Side s);
  // Give whole grid
  void getGrid(CkFuture f);
  // Get the right-hand-side of a grid
  void getRhs(CkFuture f);
  // Sets the current domain to SUM(coeffs*domains)
  void linearCombination(std::vector<double> coeffs,
			  std::vector<CProxy_Domain> domains,
			  CkCallback cb);
  // become a copy of domain d
  void copy(CProxy_Domain d, CkCallback cb);
  // increment self.grid by c*d
  void increment(double c, CProxy_Domain d, CkCallback cb);
  // set to be the right-hand-side of domain d at time t
  void setToRhs(CProxy_Domain d, CkCallback cb);
  // calculate current error
  void calculateError(double t, CkCallback cb);

  // Tell a reduction target (via callback) that you're done
  inline void declareDone(CkCallback cb);

  // Calculates the index modulo myNumElements such that negative
  // indexes index from the end of the domain
  int domainMod(int i);

  // delete self
  void deleteSelf();

  // Serialization
  void pup(PUP::er& p);

private:
  Grid myGrid;
  int myNumElements;
};

// sets domain d to sum(coeffs*domains)
class AddDomains : public CBase_AddDomains {
public:
  AddDomains(CProxy_Domain d,
	     std::vector<double> coeffs,
	     std::vector<CProxy_Domain> domains,
	     CkFuture f);
  void done(int isDone);

private:
  CkFuture doneFuture;
};

// sets d1 = d2
class CopyDomain : public CBase_CopyDomain {
public:
  CopyDomain(CProxy_Domain d1, CProxy_Domain d2, CkFuture f);
  void done(int isDone);

private:
  CkFuture doneFuture;
};

// increment d1 by c*d2
class IncrementDomain : public CBase_IncrementDomain {
public:
  IncrementDomain(CProxy_Domain d1,
		  double c, CProxy_Domain d2,
		  CkFuture f);
  void done(int isDone);

private:
  CkFuture doneFuture;
};

// set d1 to the right-hand-side of d2 at time t
class SetToRhs : public CBase_SetToRhs {
public:
  SetToRhs(CProxy_Domain d1,
	   CProxy_Domain d2,
	   CkFuture f);
  void done(int isDone);

private:
  CkFuture doneFuture;
};

// Output the domain d at time t to file
class Output : public CBase_Output {
public:
  Output(int timestep, double t,
	 CProxy_Domain d, int numElements,
	 CkFuture fCopyDone,
	 CkFuture fOutputDone);
  Output(int timestep, double t,
	 CProxy_Domain d, int numElements,
	 std::string extraLine,
	 CkFuture fCopyDone,
	 CkFuture fOutputDone);
  const std::string filePrefix = "wavetoy_output_ti_";
  const std::string filePostfix = ".dat";
  const std::string header =
    "# first line:\n#timestep t walltime\n# remaning lines:\n#x\tu\trho\n";
  const std::string whitespaceCharacter = " ";
private:
  std::ofstream outfile;
  Grid g;
  std::unique_ptr<GridMsg> mGridMsg;
  std::vector<CkFuture> gridFutures;
  std::string filename;
  CProxy_Domain dCopy;
  CkFuture copyFinished;

  std::string makeFileName(int timestep);
  std::string makeFileName(int timestep,
			   std::string extraLine);
  void outputFirstLine(int timestep, double t);
  void outputGrid();
  void prepareFutures(int numElements);
  void initiateOutput(int timestep, double t);
  void doOutput(int timestep, double t,
		CProxy_Domain d, int numElements,
		CkFuture fCopyDone,
		CkFuture fOutputDone);

};

// Calculate the L2 norm of the error in a domain d
class GetNorm2Error : public CBase_GetNorm2Error {
public:
  GetNorm2Error(double t, CProxy_Domain d, CkFuture f);
  void sumSquareErrors(double squareError);
private:
  CkFuture errorFuture;
};

#endif // INCLUDE_DOMAIN_HPP
