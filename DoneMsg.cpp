// DoneMsg.cpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-07-14 15:39:59 (jmiller)>

// a messge that simply signifies "done."

#include "DoneMsg.hpp"

DoneMsg::DoneMsg() {}

#include "donemsg.def.h"

