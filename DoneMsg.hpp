// DoneMsg.hpp
// Time-stamp: <2015-07-14 15:40:10 (jmiller)>
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

// A message with no content that simply signifies "Done."
#ifndef INCLUDE_DONE_MSG_HPP
#define INCLUDE_DONE_MSG_HPP

#include "donemsg.decl.h"

class DoneMsg : public CMessage_DoneMsg {
public:
  DoneMsg();
};

#endif // INCLUDE_DONE_MSG_HPP
