// ErrorMsg.cpp
// Time-stamp: <2015-08-07 12:21:13 (jmiller)>
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

// A message that contains a double, which is norm2 error.

#include "ErrorMsg.hpp"

ErrorMsg::ErrorMsg(const double error) {
  this->error = error;
}

#include "errormsg.def.h"
