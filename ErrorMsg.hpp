// ErrorMsg.hpp
// Time-stamp: <2015-08-07 12:20:44 (jmiller)>
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

// A message that contains a double, which is norm2 error.
#ifndef INCLUDE_ERROR_MSG_HPP
#define INCLUDE_ERROR_MSG_HPP

#include "errormsg.decl.h"

class ErrorMsg : public CMessage_ErrorMsg {
public:
  ErrorMsg(const double error);
  double error;
};

#endif // INCLUDE_ERROR_MSG_HPP
