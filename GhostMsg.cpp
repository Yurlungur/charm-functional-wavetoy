// GhostMsg.cpp
// Time-stamp: <2015-07-05 11:50:41 (jmiller)>
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

// A message class to communicate ghost data.

#include "GhostMsg.hpp"
#include <cassert>

GhostMsg::GhostMsg(const Side& s, const Grid& g) {
  int iFlattened = 0;
  sourceSide = s;
  destinationSide = (s == Side::LEFT) ? Side::RIGHT : Side::LEFT;
  for (int v = 0; v < g.NUM_VARS; ++v) {
    if (sourceSide == Side::LEFT) { 
      for (int i = 0; i < g.NUM_GHOSTS; ++i) {
	ghostData[iFlattened++] = g.get_var(v,i);
      }
    } else { // if sourceSide == Side::RIGHT
      for (int i = g.get_nx_local() - g.NUM_GHOSTS;
	   i < g.get_nx_local();
	   ++i) {
	ghostData[iFlattened++] = g.get_var(v,i);
      }
    }
  }
  assert (iFlattened == Grid::NUM_VARS*Grid::NUM_GHOSTS);
}

void GhostMsg::fillGrid(Grid& g) const {
  int iFlattened = 0;
  for (int v = 0; v < g.NUM_VARS; ++v) {
    if (destinationSide == Side::LEFT) {
      for (int i = - g.NUM_GHOSTS; i < 0; ++i) {
	g.get_var(v,i) = ghostData[iFlattened++];
      }
    } else { // destinationSide == Side::RIGHT
      for (int i = g.get_nx_local();
	   i < g.get_nx_local() + g.NUM_GHOSTS;
	   ++i) {
	g.get_var(v,i) = ghostData[iFlattened++];
      }
    }
  }
  assert (iFlattened == Grid::NUM_VARS*Grid::NUM_GHOSTS);
}

#include "ghostmsg.def.h"
