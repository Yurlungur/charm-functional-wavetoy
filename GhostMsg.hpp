// GhostMsg.hpp
// Time-stamp: <2015-07-06 10:32:43 (jmiller)>
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

// A message class to communicate ghost data

#ifndef INCLUDE_GHOST_MSG_HPP
#define INCLUDE_GHOST_MSG_HPP

#include "ghostmsg.decl.h"
#include "Grid.hpp"
#include "Sides.hpp"

class GhostMsg : public CMessage_GhostMsg {
public:
  double ghostData[Grid::NUM_VARS*Grid::NUM_GHOSTS];
  // Side to fill is the opposite of the side it comes from
  Side sourceSide;
  Side destinationSide; 
  GhostMsg(const Side& s, const Grid& g);
  void fillGrid(Grid& g) const;
};

#endif // INCLUDE_GHOST_MSG_HPP
