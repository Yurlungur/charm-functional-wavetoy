// Grid.cpp
// Time-stamp: <2015-08-06 22:46:13 (jmiller)>
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

#include "Grid.hpp"
#include <cassert>
#include <limits>
#include <pup_stl.h>

const double DNAN = std::numeric_limits<double>::signaling_NaN();
const int INAN = std::numeric_limits<int>::signaling_NaN();

Grid::Grid() 
  : Grid(DNAN,DNAN,INAN)
{
  x_data = data[0];
}

Grid::Grid(double xmin, double xmax, int nx)
  : size(nx+2*NUM_GHOSTS)
  , width(xmax-xmin)
  , dx(width/double(nx-1))
  , x_data(size)
{
  for (int i = -1; i < size - NUM_GHOSTS; ++i) {
    get_x(i) = xmin + i*dx;
  }
  for (int v = 0; v < NUM_VARS; ++v) {
    data[v] = Data(size);
  }
  set_var_names();
}

Grid::Grid(const Grid& in)
  : Grid(in.get_xmin(),in.get_xmax(),in.get_nx_local())
{
  for (int v = 0; v < NUM_VARS; ++v) {
    data[v] = in[v];
  }
  set_var_names();
}

Grid& Grid::operator= (const Grid& g) {
  size = g.get_size();
  width = g.get_xmax() - g.get_xmin();
  x_data = g.get_x();
  dx = x_data[1] - x_data[0];
  for (int v = 0; v < NUM_VARS; ++v) {
    data[v] = g[v];
  }
  set_var_names();
  return *this;
}

Grid& Grid::operator+=(const Grid& g) {
  assert (size == g.get_size());
  assert (x_data == g.get_x());
  for (int v=0; v< NUM_VARS; ++v) {
    data[v] += g[v];
  }
  return *this;
}

Grid& Grid::operator+=(const double d) {
  for (int v=0; v < NUM_VARS; ++v) data[v] += d;
  return *this;
}

Grid& Grid::operator-=(const Grid& g) {
  assert (size == g.get_size());
  assert (x_data == g.get_x());
  for (int v=0; v< NUM_VARS; ++v) {
    data[v] -= g[v];
  }
  return *this;
}

Grid& Grid::operator-=(const double d) {
  for (int v=0; v < NUM_VARS; ++v) data[v] -= d;
  return *this;
}

Grid& Grid::operator*=(const double s) {
  for (int v=0; v < NUM_VARS; ++v) data[v] *= s;
  return *this;
}

Grid Grid::operator-() const {
  Grid out(*this);
  out *= -1.0;
  return out;
}

int Grid::global_to_local(int i) const {
  assert (-NUM_GHOSTS <= i && i < size - NUM_GHOSTS);
  return i + NUM_GHOSTS;
}

// double Grid::get_x(const int i) const {
//   return x_data[global_to_local(i)];
// }

double & Grid::get_x(const int i) {
  return x_data[global_to_local(i)];
}

double Grid::get_x(const int i) const {
  return x_data[global_to_local(i)];
}

Data Grid::get_x() const {
  return x_data;
}

double & Grid::get_var(const int v, const int i) {
  return data[v][global_to_local(i)];
}

double Grid::get_var(const int v, const int i) const {
  return data[v][global_to_local(i)];
}

Data& Grid::operator[](const int v) {
  return data[v];
}

Data Grid::operator[](const int v) const {
  return data[v];
}

double Grid::get_dx() const { return dx; }

double Grid::get_xmin() const { return x_data[NUM_GHOSTS]; }

double Grid::get_xmax() const { return x_data[size - 2*NUM_GHOSTS]; }

int Grid::get_size() const { return size; }

int Grid::get_nx_local() const { return size - 2*NUM_GHOSTS; }

std::string Grid::get_name(const int v) const {
  return var_names[v];
}

double Grid::get_derivative(const int v, const int i) const {
  assert ( 0 <= i && i <= get_nx_local()
	   && "You can only differentiate non-ghost points");
  double numerator = get_var(v,i+1) - 2*get_var(v,i) + get_var(v,i-1);
  double denominator = get_dx()*get_dx();
  return numerator/denominator;
  // return(get_var(v,i+1) - get_var(v,i-1)) / (get_dx());
}

void Grid::set_var_names() {
  var_names[0] = "u";
  var_names[1] = "rho";
}

Grid operator+(const Grid& g1, const Grid& g2) {
  Grid out(g1);
  out += g2;
  return out;
}

Grid operator+ (const Grid& g1, const double d) {
  Grid out(g1);
  out += d;
  return out;
}

Grid operator+ (const double d, const Grid& g1) {
  return g1 + d;
}

Grid operator* (const double s, const Grid& g) {
  Grid out(g);
  for (int v = 0; v < g.NUM_VARS; ++v) {
    g[v] *= s;
  }
  return out;
}

Grid operator* (const Grid& g, const double s) {
  return s*g;
}

Grid operator-(const Grid& g1, const Grid& g2) {
  return g1 + -1.0*g2;
}

Grid operator-(const Grid& g, const double s) {
  Grid out(g);
  out -= s;
  return out;
}

Grid operator-(const double s, const Grid& g) {
  Grid out(g);
  out = -out;
  out += s;
  return out;
}

bool operator==(const Grid& g1, const Grid& g2) {
  if (g1.size != g2.size) return false;
  if (g1.width != g2.width) return false;
  if (g1.dx != g2.dx) return false;
  if (g1.x_data != g2.x_data) return false;
  for (int v = 0; v < Grid::NUM_VARS; ++v) {
    if (g1[v] != g2[v]) return false;
  }
  return true;
}

bool operator!=(const Grid& g1, const Grid& g2) {
  return !(g1 == g2);
}

std::ostream& operator<< (std::ostream& os, const Grid& g) {
  os << "Grid object:\n"
     << "[width,size,dx] = ["
     << g.width << ", " << g.size << ", " << g.dx << "]" << "\n"
     << "x = " << g.x_data << "\n"
     << "data:\n";
  for (int v = 0; v < g.NUM_VARS; ++v) {
    os << "\t" << g.get_name(v) << " = " << g[v] << "\n";
  }
  return os;
}

void Grid::pup(PUP::er& p) {
  p|size;
  p|width;
  p|dx;
  p|x_data;
  PUParray(p,&(var_names[0]),NUM_VARS);
  PUParray(p,&(data[0]),NUM_VARS);
}
