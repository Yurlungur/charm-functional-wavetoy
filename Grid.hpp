// Grid.hpp
// Time-stamp: <2015-08-06 22:46:16 (jmiller)>
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

// This is a class that represents a grid of points. It's templated by
// number of variables and it obeys simple arithmetic operations

#ifndef INCLUDE_GRID_HPP
#define INCLUDE_GRID_HPP


#include "Data.hpp"
#include <memory>
#include <iostream>
#include <string>
#include <pup.h>

class Grid {
// TODO: These could be templated.
public:
  static const int DIMENSIONS = 1;
  static const int NUM_VARS = 2; // u and rho = du/dt
  static const int NUM_GHOSTS = 1; // on the left and right-hand side
				   // of each grid

  // The current time on the grid.
  double time = 0;

public:
  // Copy constructor
  Grid(const Grid& in);
  // Constructor creates an empty grid
  // ranging from xmin to xmax with nx grid points
  Grid(double xmin, double xmax, int nx);
  // Empty constructor
  Grid();
  
  // Getters
  double get_dx() const;
  double get_xmin() const;
  double get_xmax() const;
  int get_size() const;
  int get_nx_local() const;
  std::string get_name(const int v) const;

  // Accesses the variable indexed by v
  Data& operator[](const int v);
  Data operator[](const int v) const;
  // Accesses the variable v at grid point i
  double & get_var(const int v, const int i);
  double get_var(const int v, const int i) const;
  // double get_x(const int i) const;
  double & get_x(const int i);
  double get_x(const int i) const;
  Data get_x() const;

  // Get the derivative of variable v at index i
  // using second-order derivatives
  double get_derivative(const int v, const int i) const;

  // Operators
  Grid& operator= (const Grid& g);
  Grid& operator+= (const Grid& g);
  Grid& operator+= (const double d);
  Grid& operator-= (const Grid& g);
  Grid& operator-= (const double d);
  Grid& operator*= (const double s);

  Grid operator- () const;

  friend Grid operator+ (const Grid& g1, const Grid& g2);
  friend Grid operator+ (const Grid& g1, const double d);
  friend Grid operator+ (const double d, const Grid& g1);

  friend Grid operator* (const double s, const Grid& g);
  friend Grid operator* (const Grid& g, const double s);

  friend Grid operator-(const Grid& g1, const Grid& g2);
  friend Grid operator-(const Grid& g, const double s);
  friend Grid operator-(const double s, const Grid& g);
  
  friend bool operator==(const Grid& g1, const Grid& g2);
  friend bool operator!=(const Grid& g1, const Grid& g2);

  friend std::ostream& operator<< (std::ostream& os, const Grid& g);

  // Charm++ serialization
  void pup(PUP::er& p);
  
private:
  int size;
  double width;
  double dx;
  Data x_data;
  std::array<std::string,NUM_VARS> var_names;
  std::array<Data, NUM_VARS> data;
  
  int global_to_local(int i) const;
  void set_var_names();
};

#endif // INCLUDE_GRID_HPP
