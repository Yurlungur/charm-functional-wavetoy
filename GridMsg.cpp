// GridMsg.cpp
// Time-stamp: <2015-08-06 15:38:29 (jmiller)>
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

// A message class for encapsulating a whole grid

#include "GridMsg.hpp"
#include <cassert>

const bool DEBUGGING = false;

GridMsg::GridMsg(const Grid& g) {
  int iFlattened = 0;
  time = g.time;
  xmin = g.get_xmin();
  xmax = g.get_xmax();
  nx = g.get_nx_local();
  if (DEBUGGING) ckout << "filling grid message" << endl;
  for (int v = 0; v < g.NUM_VARS; ++v) {
    for (int i = 0; i < g.get_size(); ++i) {
      flattenedData[iFlattened++] = g[v][i];
      if (DEBUGGING) ckout << g[v][i] << " "<< endl;
    }
  }
  assert( iFlattened = g.NUM_VARS*g.get_size() );
}

void GridMsg::fillGrid(Grid& g) const {
  int iFlattened = 0;
  g = Grid(xmin,xmax,nx);
  g.time = time;
  if (DEBUGGING) ckout << "filling grid" << endl;
  for (int v = 0; v < g.NUM_VARS; ++v) {
    for (int i = 0; i < g.get_size(); ++i) {
      if (DEBUGGING) ckout << flattenedData[iFlattened] << " " << endl;
      g[v][i] = flattenedData[iFlattened++];
    }
  }
  assert( iFlattened = g.NUM_VARS*g.get_size() );
}

#include "gridmsg.def.h"
