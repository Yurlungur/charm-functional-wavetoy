// GridMsg.hpp
// Time-stamp: <2015-07-09 01:44:07 (jmiller)>
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

// A message class for encapsulating a whole grid.

#ifndef INCLUDE_GRID_MSG_HPP
#define INCLUDE_GRID_MSG_HPP

#include "Grid.hpp"
#include "gridmsg.decl.h"

class GridMsg : public CMessage_GridMsg {
public:
  double* flattenedData;
  double time;
  double xmin;
  double xmax;
  int nx;
  GridMsg(const Grid& g);
  void fillGrid(Grid& g) const;
};

#endif // INCLUDE_GRID_MSG_HPP
