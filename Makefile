CXX=/home/jmiller/programming/charm/bin/charmc
CXXFLAGS = -g -std=c++11 # -Wall

MAIN_MODULE=wavetoy
GHOSTMSG_MODULE=ghostmsg
GRIDMSG_MODULE=gridmsg
DONEMSG_MODULE=donemsg
ERRORMSG_MODULE=errormsg
UNITTESTS_MODULE=unittests
DOMAIN_MODULE=domain

INTERFACE_FILES=${MAIN_MODULE}.ci\
		${GHOSTMSG_MODULE}.ci\
		${GRIDMSG_MODULE}.ci\
		${UNITTESTS_MODULE}.ci\
		${DONEMSG_MODULE}.ci\
		${ERRORMSG_MODULE}.ci\
		${DOMAIN_MODULE}.ci

MAIN_CHARM_FILES=${MAIN_MODULE}.def.h ${MAIN_MODULE}.decl.h
GHOSTMSG_CHARM_FILES=${GHOSTMSG_MODULE}.def.h ${GHOSTMSG_MODULE}.decl.h
GRIDMSG_CHARM_FILES=${GRIDMSG_MODULE}.def.h ${GRIDMSG_MODULE}.decl.h
UNITTESTS_CHARM_FILES=${UNITTESTS_MODULE}.def.h ${UNITTESTS_MODULE}.decl.h
DONEMSG_CHARM_FILES=${DONEMSG_MODULE}.def.h ${DONEMSG_MODULE}.decl.h
ERRORMSG_CHARM_FILES=${ERRORMSG_MODULE}.def.h ${ERRORMSG_MODULE}.decl.h
DOMAIN_CHARM_FILES=${DOMAIN_MODULE}.def.h ${DOMAIN_MODULE}.decl.h

MSG_CHARM_FILES=${GHOSTMSG_CHARM_FILES}\
		${GRIDMSG_CHARM_FILES}\
		${DONEMSG_CHARM_FILES}\
		${ERRORMSG_CHARM_FILES}

CHARM_FILES=${MAIN_CHARM_FILES}\
		${MSG_CHARM_FILES}\
		${UNITTESTS_CHARM_FILES}\
		${DOMAIN_CHARM_FILES}

DATA_STRUCTURE_HEADERS=Data.hpp Grid.hpp

DATA_STRUCTURE_OBJECTS=Data.o Grid.o
TEST_OBJECTS=wavetoy_unit_tests.o
GHOSTMSG_OBJECTS=GhostMsg.o
GRIDMSG_OBJECTS=GridMsg.o
UNITTEST_OBJECTS=unittests.o
DOMAIN_OBJECTS=Domain.o
ERRORMSG_OBJECTS=ErrorMsg.o
MAIN_OBJECTS=charm_functional_wavetoy.o

OBJECT_FILES=${DATA_STRUCTURE_OBJECTS}\
		${GHOSTMSG_OBJECTS}\
		${GRIDMSG_OBJECTS}\
		${UNITTEST_OBJECTS}\
		${ERRORMSG_OBJECTS}\
		AnalyticSolution.o\
		${DOMAIN_OBJECTS}\
		DoneMsg.o


MAIN_LOOPS=wavetoy_unit_tests.o charm_functional_wavetoy.o
BIN_FILES=wavetoy_unit_tests.bin charm_functional_wavetoy.bin

default: test

all: charm_unit_tests.bin charm_functional_wavetoy.bin

run: charm_functional_wavetoy.bin
	./charmrun ./charm_functional_wavetoy.bin +p4 ++local +isomalloc_sync

test: wavetoy_unit_tests.bin
	./charmrun ./wavetoy_unit_tests.bin +p4 ++local +isomalloc_sync

charm_functional_wavetoy.bin: $(OBJECT_FILES) $(MAIN_OBJECTS)
	$(CXX) $(CXXFLAGS) -o $@ $^

wavetoy_unit_tests.bin: $(OBJECT_FILES) $(TEST_OBJECTS)
	$(CXX) $(CXXFLAGS) -o $@ $^

charm_functional_wavetoy.o: charm_functional_wavetoy.hpp \
				charm_functional_wavetoy.cpp\
				$(DATA_STRUCTURE_HEADERS)\
				charmstuff

wavetoy_unit_tests.o: wavetoy_unit_tests.cpp $(DATA_STRUCTURE_HEADERS) \
			unittests.hpp charmstuff

Grid.o: Grid.cpp Grid.hpp Data.hpp

Data.o: Data.cpp Data.hpp

unittests.o: unittests.cpp unittests.hpp \
	${UNITTESTS_CHARM_FILES}\
	${GHOSTMSG_CHARM_FILES}\
	${DONEMSG_CHARM_FILES}

GridMsg.o: GridMsg.hpp GridMsg.cpp ${GRIDMSG_CHARM_FILES}

GhostMsg.o: GhostMsg.cpp GhostMsg.hpp ${GHOSTMSG_CHARM_FILES}

DoneMsg.o: DoneMsg.cpp DoneMsg.hpp ${DONEMSG_CHARM_FILES}

ErrorMsg.o: ErrorMsg.cpp ErrorMsg.hpp ${ERRORMSG_CHARM_FILES}

Domain.o: Domain.hpp Grid.hpp Sides.hpp\
		AnalyticSolution.hpp Variables.hpp \
		${DOMAIN_CHARM_FILES} ${MSG_CHARM_FILES}

AnalyticSolution.o: AnalyticSolution.cpp AnalyticSolution.hpp Grid.hpp

charmstuff: $(CHARM_FILES)

${ERRORMSG_MODULE}.def.h: ${ERRORMSG_MODULE}.ci
	$(CXX) $^

${ERRORMSG_MODULE}.decl.h: ${ERRORMSG_MODULE}.ci
	$(CXX) $^

${DONEMSG_MODULE}.def.h: ${DONEMSG_MODULE}.ci
	$(CXX) $^

${DONEMSG_MODULE}.decl.h: ${DONEMSG_MODULE}.ci
	$(CXX) $^

${GRIDMSG_MODULE}.def.h: ${GRIDMSG_MODULE}.ci
	$(CXX) $^

${GRIDMSG_MODULE}.decl.h: ${GRIDMSG_MODULE}.ci
	$(CXX) $^

${DOMAIN_MODULE}.def.h: ${DOMAIN_MODULE}.ci
	$(CXX) $^

${DOMAIN_MODULE}.decl.h: ${DOMAIN_MODULE}.ci
	$(CXX) $^

${UNITTESTS_MODULE}.decl.h: ${UNITTESTS_MODULE}.ci
	$(CXX) $^

${UNITTESTS_MODULE}.def.h: ${UNITTESTS_MODULE}.ci
	$(CXX) $^

${GHOSTMSG_MODULE}.def.h: ${GHOSTMSG_MODULE}.ci
	$(CXX) $^

${GHOSTMSG_MODULE}.decl.h: ${GHOSTMSG_MODULE}.ci
	$(CXX) $^

${MAIN_MODULE}.decl.h: ${MAIN_MODULE}.ci
	$(CXX) $^

${MAIN_MODULE}.def.h: ${MAIN_MODULE}.ci
	$(CXX) $^

.PHONY: default test charmstuff clean

clean:
	$(RM) $(CHARM_FILES) $(OBJECT_FILES) $(MAIN_LOOPS) $(BIN_FILES) charmrun *.dat
