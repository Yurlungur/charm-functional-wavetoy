// Sides.hpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
//Time-stamp: <2015-07-09 01:33:48 (jmiller)>

// A simple name space for defining sides.

#ifndef INCLUDED_SIDES_HPP
#define INCLUDED_SIDES_HPP

#include <pup.h>

enum class Side{LEFT, RIGHT};

// HAS to be inline, or you get
// compile time errors.
// So says the charm++ mailing list.
// wtf. Black magic.
inline void operator|(PUP::er& p, Side& s) {
  pup_bytes(&p, (void*)&s, sizeof(Side));
}

#endif // INCLUDED_SIDES_HPP
