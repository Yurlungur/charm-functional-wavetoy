// Variables.hpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-08-01 10:18:07 (jmiller)>

// A simple name space for variable names

#ifndef INCLUDED_VARIABLES_HPP
#define INCLUDED_VARIABLES_HPP

namespace Variables{
  const int U = 0;
  const int RHO = 1;
}

#endif // INCLUDED_VARIABLES_HPP
