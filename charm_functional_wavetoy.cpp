// charm_functional_wavetoy.cpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-08-07 18:01:22 (jmiller)>

// This is the implementation of the main chare that evolves the
// system.

#include "charm_functional_wavetoy.hpp"
#include "math.h"
#include <memory>

const bool DEBUGGING = false;

Main::Main(CkArgMsg* msg)
  : coeffs(NUM_RK_COEFFS)
  , domains(NUM_RK_COEFFS)
{
  ckout << "Welcome to charm functional wavetoy."
	<< endl;
  processCliArgs(msg);
  ckout << "Number of elements = "
	<< numElements << endl;
  ckout << "Number of grid points per element = "
	<< nxPerElement << endl;

  ckout << "Creating chare arrays." << endl;
  y = CProxy_Domain::ckNew(nxPerElement,
			   numElements,
			   numElements);
  opts = CkArrayOptions(numElements);
  opts.bindTo(y);
  k1 = CProxy_Domain::ckNew(nxPerElement,
			    numElements,
			    opts);
  k1Point5 = CProxy_Domain::ckNew(nxPerElement,
				  numElements,
				  opts);
  k2 = CProxy_Domain::ckNew(nxPerElement,
			    numElements,
			    opts);
  ckout << "running..." << endl;

  thisProxy.run();
}

void Main::processCliArgs(CkArgMsg* msg) {
  int numArgs = msg -> argc;
  // argv[0] is system name
  if ( numArgs >= 3 ) nxPerElement = atoi(msg -> argv[2]);
  else nxPerElement = DEFAULT_NX_PER_ELEMENT;
  if ( numArgs >= 2 ) numElements = atoi(msg -> argv[1]);
  else  numElements = CkNumPes();
  totalNx = nxPerElement*numElements;
  dx = (AnalyticSolution::GLOBAL_XMAX
	-AnalyticSolution::GLOBAL_XMIN)/totalNx;
  dt = CFL_FACTOR*dx;
  numTimeSteps = (int)ceil((FINAL_TIME - INITIAL_TIME) / dt) + 1;
  coeffs[0] = 1.0;
  coeffs[1] = 0.5*dt;
}

void Main::rk2Step() {
  if (DEBUGGING) ckout << "rk2 step started" << endl;
  k1Future = CkCreateFuture();

  if (DEBUGGING) ckout << "Calculating k1" << endl;
  start = CkWallTimer();
  CProxy_SetToRhs::ckNew(k1,y,k1Future);

  domains[0] = y;
  domains[1] = k1;
  k1Point5Future = CkCreateFuture();
  k2Future = CkCreateFuture();
  incrementFuture = CkCreateFuture();
  if (DEBUGGING) ckout << "Waiting for k1 finish." << endl;
  isDone.reset( (DoneMsg*) CkWaitFuture(k1Future) );
  end = CkWallTimer();
  total_time_in_rhs += (end - start);
  total_rhs_evaluations += 1;

  if (DEBUGGING) ckout << "Calculating k1point5" << endl;
  CProxy_AddDomains::ckNew(k1Point5,
			   coeffs,domains,
			   k1Point5Future);
  isDone.reset( (DoneMsg*) CkWaitFuture(k1Point5Future) );

  if (DEBUGGING) ckout << "calculating k2" << endl;
  start = CkWallTimer();
  CProxy_SetToRhs::ckNew(k2,k1Point5,k2Future);
  isDone.reset( (DoneMsg*) CkWaitFuture(k2Future) );
  end = CkWallTimer();
  total_time_in_rhs += (end - start);
  total_rhs_evaluations += 1;

  if (DEBUGGING) ckout << "updating y" << endl;
  CProxy_IncrementDomain::ckNew(y,dt,k2,incrementFuture);

  CkReleaseFuture(k1Future);
  CkReleaseFuture(k1Point5Future);
  CkReleaseFuture(k2Future);

  if (OUTPUT_RHS) outputRhs();  

  isDone.reset( (DoneMsg*) CkWaitFuture(incrementFuture) );

  currentTime += dt;
  currentTimeStep += 1;
  if (DEBUGGING) ckout << "rk2 step ended" << endl;
}

void Main::outputRhs() {
  for (int i = 0; i < NUM_RK_SUBSTEPS; ++i) {
    rhsCopyFutures[i] = CkCreateFuture();
    rhsOutputFutures[i] = CkCreateFuture();
  }
  CProxy_Output::ckNew(currentTimeStep,currentTime,
		       k1,numElements,"k1",
		       rhsCopyFutures[0],rhsOutputFutures[0]);
  CProxy_Output::ckNew(currentTimeStep,currentTime,
		       k1Point5,numElements,"k1Point5",
		       rhsCopyFutures[1],rhsOutputFutures[1]);
  CProxy_Output::ckNew(currentTimeStep,currentTime,
		       k2,numElements,"k2",
		       rhsCopyFutures[2],rhsOutputFutures[2]);
  for (int i = 0; i < NUM_RK_SUBSTEPS; ++i) {
    if (currentTimeStep >= numTimeSteps - (OUTPUT_EVERY+1)) {
      CkWaitFuture(rhsOutputFutures[i]);
    } else {
      CkWaitFuture(rhsCopyFutures[i]);
    }
    CkReleaseFuture(rhsCopyFutures[i]);
    CkReleaseFuture(rhsOutputFutures[i]);
  }
}

void Main::integrate() {
  ckout << "Beginning ntegration..." << endl;
  while (currentTimeStep < numTimeSteps) {
    if (DEBUGGING) {
      ckout << "Timestep = " << currentTimeStep
	    << "\n"
	    << "Time = " << currentTime
	    << endl;
    }
    if (currentTimeStep % OUTPUT_EVERY == 0) output();
    rk2Step();
    if (DEBUGGING) ckout << "looping over to next iteration" << endl;
  }

}

void Main::output() {
  if (OUTPUT_DOMAINS) {
    CkReleaseFuture(copyFuture);
    CkReleaseFuture(outputFuture);
    copyFuture = CkCreateFuture();
    outputFuture = CkCreateFuture();
    if (DEBUGGING) ckout << "Outputting current data..." << endl;
    CProxy_Output::ckNew(currentTimeStep,currentTime,
			 y,numElements,
			 copyFuture,outputFuture);
    if (currentTimeStep >= numTimeSteps - (OUTPUT_EVERY+1)) {
      outputFinished.reset((DoneMsg*) CkWaitFuture(outputFuture));
    } else {
      outputFinished.reset((DoneMsg*) CkWaitFuture(copyFuture));
    }
  }
  if (OUTPUT_ERROR) printError();
}

void Main::printError() {
  CkReleaseFuture(errorFuture);
  errorFuture = CkCreateFuture();
  CProxy_GetNorm2Error::ckNew(currentTime,y,errorFuture);
  norm2ErrorMsg.reset( (ErrorMsg*) CkWaitFuture(errorFuture) );
  ckout << "L2 Error at time t = "
	<< currentTime << " is "
	<< norm2ErrorMsg->error
	<< endl;
}

void Main::run() {
  integrate();
  ckout << "Integration complete!" << endl;
  printError();
  ckout<< "Average time per rhs-evaluation: "
       << total_time_in_rhs / total_rhs_evaluations
       << " seconds."
       << endl;
  CkExit();
}

#include "wavetoy.def.h"
