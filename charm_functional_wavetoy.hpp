// charm_functional_wavetoy.hpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-08-07 18:06:28 (jmiller)>

// This is the header for an implementation of the main chare that
// evolves the system.

#ifndef INCLUDED_CHARM_FUNCTIONAL_WAVETOY
#define INCLUDED_CHARM_FUNCTIONAL_WAVETOY

#include "wavetoy.decl.h"
#include "GhostMsg.hpp"
#include "GridMsg.hpp"
#include "Data.hpp"
#include "Grid.hpp"
#include "Sides.hpp"
#include "unittests.hpp"
#include "DoneMsg.hpp"
#include "Domain.hpp"
#include "ErrorMsg.hpp"
#include "AnalyticSolution.hpp"

#include <memory>
#include <string>
#include <sstream>

class Main : public CBase_Main {
public:
  static constexpr int DEFAULT_NX_PER_ELEMENT = 5;
  static constexpr int OUTPUT_EVERY = 1;
  static constexpr double INITIAL_TIME = 0;
  static constexpr double FINAL_TIME = 10.0;
  static constexpr double CFL_FACTOR = 0.5;
  static constexpr int NUM_RK_SUBSTEPS = 3;
  static constexpr int NUM_RK_COEFFS = 2;
  static constexpr bool OUTPUT_RHS = false;
  static constexpr bool OUTPUT_DOMAINS = false;
  static constexpr bool OUTPUT_ERROR = false;

  Main(CkArgMsg* msg);
  void run();

private:
  void processCliArgs(CkArgMsg* msg);
  void rk2Step();
  void integrate();
  void output();
  void outputRhs();
  void printError();

  double start,end;
  int total_rhs_evaluations=0;
  double total_time_in_rhs=0;
  
  int nxPerElement;
  int numElements;
  double totalNx;
  double dx;
  double dt;

  double currentTime = INITIAL_TIME;
  int currentTimeStep = 0;
  int numTimeSteps;
  CProxy_Domain y;
  CProxy_Domain k1;
  CProxy_Domain k1Point5;
  CProxy_Domain k2;
  CkArrayOptions opts;

  CkFuture k1Future;
  CkFuture k1Point5Future;
  CkFuture k2Future;
  CkFuture incrementFuture;

  std::vector<double> coeffs;
  std::vector<CProxy_Domain> domains;
  std::array<CkFuture,NUM_RK_SUBSTEPS> rhsCopyFutures;
  std::array<CkFuture,NUM_RK_SUBSTEPS> rhsOutputFutures;
  std::unique_ptr<DoneMsg> outputFinished;
  std::unique_ptr<DoneMsg> isDone;
  std::unique_ptr<ErrorMsg> norm2ErrorMsg;
  CkFuture copyFuture = CkCreateFuture();
  CkFuture outputFuture = CkCreateFuture();
  CkFuture errorFuture = CkCreateFuture();
};

#endif // INCLUDED_CHARM_FUNCTIONAL_WAVETOY
