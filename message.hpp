#ifndef INCLUDE_MESSAGE_HPP
#define INCLUDE_MESSAGE_HPP

#include "message.decl.h"
#include <vector>

class VarSizeMsg : public CMessage_VarSizeMsg {
public:
  double* data;
  int size;
  VarSizeMsg(const std::vector<double>& v);
  void fillVector(std::vector<double>& v) const;
};



#endif // INCLUDE_MESSAGE_HPP
