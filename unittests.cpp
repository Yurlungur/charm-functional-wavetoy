// unittests.cpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-07-14 13:48:54 (jmiller)>

// This implementation file contains information for the unittests module

#include "unittests.hpp"
#include "GhostMsg.hpp"
#include "GridMsg.hpp"
#include "Data.hpp"
#include "DoneMsg.hpp"
#include <string>
#include <sstream>
using std::stringstream;

TestChare::TestChare(double xmin, double xmax, int nx,
		     double fillValue,
		     double leftUGhost, double rightUGhost,
		     double leftRhoGhost, double rightRhoGhost)
  : g(xmin,xmax,nx)
{
  stringstream ss;
  for (int v = 0; v < g.NUM_VARS; ++v) {
    g[v] = Data(g[v].size(),fillValue);
  }
  g[0][1] = leftUGhost;
  g[0][g[0].last_index()-1] = rightUGhost;
  g[1][1] = leftRhoGhost;
  g[1][g[1].last_index()-1] = rightRhoGhost;
  ss.str("");
  ss << g;
  ckout << "TestChare initialized. The grid is:\n"
	<< ss.str().c_str()
	<< endl;
}

void TestChare::getGhost(CkFuture f, Side s) {
  GhostMsg* m = new GhostMsg(s,g);
  CkSendToFuture(f,m);
}

void TestChare::getGrid(CkFuture f) {
  GridMsg* m = new (g.NUM_VARS*g.get_size()) GridMsg(g);
  CkSendToFuture(f,m);
}

void TestChare::isDone(CkFuture f) {
  DoneMsg* m = new DoneMsg();
  CkSendToFuture(f,m);
}

void TestChare::deleteSelf() {
  delete this; // only way I can figure out how to get a chare to
	       // delete itself.
}

#include "unittests.def.h"
