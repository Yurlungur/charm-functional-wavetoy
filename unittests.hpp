// unittests.hpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-07-14 13:48:10 (jmiller)>

// This header file contains information for the unit tests module.

#ifndef INCLUDE_UNITTESTS_HPP
#define INCLUDE_UNITTESTS_HPP

#include "unittests.decl.h"
#include "Sides.hpp"
#include "Grid.hpp"

class TestChare : public CBase_TestChare {
private:
  Grid g;
public:
  TestChare(double xmin, double xmax, int nx,
	    double fillValue,
	    double leftUGhost, double rightUGhost,
	    double leftRhoGhost, double rightRhoGhost);
  //void getLeftGhost(CkFuture f);
  //void getRightGhost(CkFuture f);
  void getGrid(CkFuture f);
  void deleteSelf();
  void getGhost(CkFuture f, Side s);
  void isDone(CkFuture f);
};

#endif // INCLUDE_UNITTESTS_HPP
