// wavetoy.cpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-07-12 19:54:50 (jmiller)>

// This implements the main chare

#include "wavetoy.hpp"
#include "AnalyticSolution.hpp"
using AnalyticDomain::GLOBAL_XMAX;
using AnalyticDomain::Global_XMIN;

Main::Main(CkArgMsg* msg) {
  processCommandLine(msg);
  globalNumGridPoints = numElements*localNumGridPoints;
  dx = (GLOBAL_XMAX - GLOBAL_XMIN)/globalNumGridPoints;
  
}

void Main::processCommandLine(CkArgMsg* msg) {
  int numArgs = msg->argc -1;
  localNumGridPoints = atoi(m->argv[1]);
  if (numArgs >= 2) {
    numElements = atoi(m->argv[2]);
  } else {
    numElements = CkNumPes();
  }
}

#include "wavetoy.def.h"
