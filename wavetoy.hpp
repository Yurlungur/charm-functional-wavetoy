// wavetoy.hpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-07-11 21:26:51 (jmiller)>

// This is the prototype of the main chare

#ifndef INCLUDE_WAVETOY_MAIN_HPP
#define INCLUDE_WAVETOY_MAIN_HPP

#include "wavetoy.decl.h"
#include "GhostMsg.hpp"
#include "GridMsg.hpp"
#include "Data.hpp"
#include "Grid.hpp"
#include "Sides.hpp"
#include <memory>

class Main : public CBase_Main {
public:
  const static int NUM_SUBSTEPS=3;
  Main(CkArgMsg* msg);
  void run();
private:
  int numElements;
  int globalNumGridPoints;
  int localNumGridPoints;
  double elementWidth;
  double dx;
  CProxy_Domain domainProxy;
  std::array<CProxy_Domain,NUM_SUBSTEPS> kProxyDomains;
  void processCommandLine(CkArgMsg* msg);
};

#endif // INCLUDE_WAVETOY_MAIN_HPP
