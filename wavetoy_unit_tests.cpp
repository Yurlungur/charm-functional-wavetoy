// wavetoy_unit_tests.cpp
// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
//Time-stamp: <2015-08-06 15:41:33 (jmiller)>

// This is an implementation of the main chare that runs unit tests on
// various components.

#include "wavetoy.decl.h"
#include "GhostMsg.hpp"
#include "GridMsg.hpp"
#include "Data.hpp"
#include "Grid.hpp"
#include "Sides.hpp"
#include "unittests.hpp"
#include "DoneMsg.hpp"
#include "Domain.hpp"

#include <memory>
#include <string>
#include <sstream>

using std::stringstream;

class Main : public CBase_Main {
private:
  const int s1 = 3;
  const int s2 = 4;
  const int s3 = 5;
public:
  Main(CkArgMsg* msg) {
    thisProxy.run();
  }
  void run() {
    ckout << "Beginning tests.\n" << endl;

    testData();
    testGrid();
    testGhostMsg();
    testGridMsg();
    testDoneMsg();
    testDomain();

    CkExit();
  }
  void testData();
  void testGrid();
  void testGhostMsg();
  void testGridMsg();
  void testDoneMsg();
  void testDomain();
};

void Main::testData() {
  stringstream ss;
  ckout << "Testing the Data object." << endl;

  ckout << "d1 = range(3)" << endl;
  Data d1(s1);
  for (int i = 0; i < d1.size(); ++i) d1[i] = i;
  ckout << "d2 = copy(d1)" << endl;
  Data d2(d1);
  Data d3 = d1 + 2*d2;
  ss.str("");
  ss << d3;
  ckout << "d3 = " << ss.str().c_str() << endl;


  ckout << "d5 = {10,10,10,10}" << endl;
  Data d5(s2,10);

  ckout << "d5[0] = " << d5[0] << endl;

  Data d6(s3);
  for (int i = 0; i < d6.size(); ++i) d6[i] = i;
  ckout << "Testing a finite differences operation:\n"
        << "d6 = [";
  for (int i = 0; i < d6.size(); ++i) ckout << d6[i] << " ";
  ckout <<"]." << endl;
  ckout << "Calculating d6[1:] - d6[:-1]." << endl;
  Data d7 = d6.slice(1,d6.size()) - d6.slice(0,d6.last_index());
  ckout << "Result = [ ";
  for (int i = 0; i < d7.size(); ++i) ckout << d7[i] << " ";
  ckout << "]." << endl;

  Data d8(4,1);
  ckout << "Result == [1,1,1,1] (true or false): "
        << (d7 == d8) << endl;

  ckout << endl;
}

void Main::testGrid() {
  stringstream ss;
  ckout << "Testing the Grid object." << endl;
  ckout << "g1 ranges from x = 0 to x = 1 with 5 grid points." << endl;
  Grid g1 = Grid(0,1,5);
  ss.str("");
  ss << g1;
  ckout << "g1 is:\n" << ss.str().c_str() << endl;

  ckout << "Setting g1 to be 0.5 for both u and rho." << endl;
  for (int v = 0; v < Grid::NUM_VARS; ++v) {
    g1[v] = Data(g1[v].size(),1) - Data(g1[v].size(),0.5);
  }
  ckout << "And multiplying by 3..." << endl;
  g1 *= 3.0;

  ckout << "Now, x = " << g1.get_xmin() << " to " << g1.get_xmax() << endl;
  ckout << "g2 = copy(g1)" << endl;
  Grid g2(g1);
  ckout << "so -g2+3is:\n";
  ss.str("");
  ss << -g2+3;
  ckout << ss.str().c_str() << endl;
}

void Main::testGhostMsg() {
  stringstream ss;
  const double xmin = 0;
  const double xmax = 1;
  const double nx = 5;
  const double fillValue = 0;
  const double ghosts[4] = {1,2,3,4};

  ckout << "Testing the Ghost Message." << endl;
  ckout << "Creating a test chare." << endl;
  CProxy_TestChare testChare = CProxy_TestChare::ckNew(xmin,xmax,nx,
                                                       fillValue,
                                                       ghosts[0],
                                                       ghosts[1],
                                                       ghosts[2],
                                                       ghosts[3]);

  ckout << "Now requesting information from the test chare.\n"
        << "we'll get the information when we need it." << endl;
  CkFuture fLeft = CkCreateFuture();
  CkFuture fRight = CkCreateFuture();
  testChare.getGhost(fLeft,Side::RIGHT);
  testChare.getGhost(fRight,Side::LEFT);

  ckout << "Creating my own grid to fill." << endl;
  Grid g(xmin,xmax,nx);
  ckout << "My grid will have NaN's everywhere\n"
        << "until it is filled by the test proxy."
        << endl;
  ss.str("");
  ss << g;
  ckout << "Here it is:\n" << ss.str().c_str() << endl;

  ckout << "Now I need the future information, so I'll wait for it get here.\n"
        << "\tI am using unique pointers to handle the messages."
        << endl;
  std::unique_ptr<GhostMsg> mLeft( (GhostMsg*) CkWaitFuture(fLeft));
  ckout << "Left future has arrived. Filling the grid." << endl;
  mLeft->fillGrid(g);
  ss.str(""); ss << g;
  ckout << "Now the grid is:\n" << ss.str().c_str() << endl;
  std::unique_ptr<GhostMsg> mRight( (GhostMsg*) CkWaitFuture(fRight));
  ckout << "Right future has arrived. Filling the grid." << endl;
  mRight->fillGrid(g);
  ss.str(""); ss << g;
  ckout << "Now the grid is:\n" << ss.str().c_str() << endl;
  CkReleaseFuture(fLeft);
  CkReleaseFuture(fRight);
  testChare.deleteSelf();
}

void Main::testGridMsg() {
  stringstream ss;
  const double xmin = 0;
  const double xmax = 1;
  const double nx = 5;
  const double fillValue = 0;
  const double ghosts[4] = {1,2,3,4};

  ckout << "Now testing the grid message." << endl;

  ckout << "Creating a test chare." << endl;
  CProxy_TestChare testChare = CProxy_TestChare::ckNew(xmin,xmax,nx,
                                                       fillValue,
                                                       ghosts[0],
                                                       ghosts[1],
                                                       ghosts[2],
                                                       ghosts[3]);

  ckout << "Asking the test chare\n"
        << "for the entire grid."
        << endl;
  CkFuture f = CkCreateFuture();
  testChare.getGrid(f);

  ckout << "Grid requested. Now we create a grid to fill." << endl;
  Grid copyGrid;//(xmin,xmax,nx);
  ss.str("");
  ss << copyGrid;
  ckout << "Initially the grid to fill looks like this:\n"
        << ss.str().c_str()
        << endl;
  GridMsg* mpGridMsg = (GridMsg*) CkWaitFuture(f);
  ckout << "The future has arrived. Filling the grid."
        << endl;
  mpGridMsg->fillGrid(copyGrid);
  ss.str("");
  ss << copyGrid;
  ckout << "Okay. The grid is:\n"
        << ss.str().c_str()
        << endl;
  delete mpGridMsg;
  ckout << "Done with this grid message." << endl;
}

void Main::testDoneMsg() {
  const double xmin = 0;
  const double xmax = 1;
  const double nx = 5;
  const double fillValue = 0;
  const double ghosts[4] = {1,2,3,4};

  ckout << "Now testing the done message." << endl;
  ckout << "Creating a test chare." << endl;
  CProxy_TestChare testChare = CProxy_TestChare::ckNew(xmin,xmax,nx,
                                                       fillValue,
                                                       ghosts[0],
                                                       ghosts[1],
                                                       ghosts[2],
                                                       ghosts[3]);
  ckout << "Asking the test chare if it's done." << endl;
  CkFuture f = CkCreateFuture();
  testChare.isDone(f);
  ckout << "Waiting for the chare to be done." << endl;
  DoneMsg* testDone = (DoneMsg*) CkWaitFuture(f);
  ckout << "Okay, it's done. Deleting the message and the chare." << endl;
  delete testDone;
  testChare.deleteSelf();
}

void Main::testDomain() {
  int nx = 5;
  int numElements = 5;
  int numDomains = 3;
  int numFutures = 15;
  stringstream ss;
  ss.str("");

  ckout << "Now testing the domain chares." << endl;
  ckout << "Creating " << numDomains << " test domains." << endl;
  ckout << "nx = " << nx << ", numElements = " << numElements << endl;
  std::vector<CProxy_Domain> domains(numDomains);
  for (int i = 0; i < numDomains; ++i) {
    domains[i] = CProxy_Domain::ckNew(nx,numElements,numElements);
  }
  std::vector<CkFuture> f(numFutures);
  for (int i = 0; i < numFutures; ++i) f[i] = CkCreateFuture();
  CProxy_Output::ckNew(0,0,domains[0],numElements,f[1],f[2]);
  ckout << "Now waiting for output." << endl;
  std::unique_ptr<DoneMsg> mDone( (DoneMsg*) CkWaitFuture(f[1]) );
  ckout << "The output chare has finish copying." << endl;
  ckout << "Let's also increment domain 1 by domain 2."
	<< endl;
  CkFuture fInc = CkCreateFuture();
  CProxy_IncrementDomain(domains[0],1.0,domains[1],fInc);
  std::unique_ptr<DoneMsg> incDone( (DoneMsg*) CkWaitFuture(fInc) );
  ckout << "And now let's output it again!" << endl;
  CProxy_Output::ckNew(1,0,domains[0],numElements,f[3],f[4]);
  std::unique_ptr<DoneMsg> outDone( (DoneMsg*) CkWaitFuture(f[3]) );
  ckout << "Now let's set domain 1 to domain 2 + 2*domain 3." << endl;
  std::vector<double> coeffs(2); coeffs[0] = 0.5; coeffs[1] =3.5;
  std::vector<CProxy_Domain> ld(2);
  for (int i = 0; i < 2; ++i) ld[i] = domains[i+1];
  CProxy_AddDomains(domains[0],coeffs,ld,f[5]);
  std::unique_ptr<DoneMsg> linCombDone( (DoneMsg*) CkWaitFuture(f[5]) );
  ckout << "And output again" << endl;
  CProxy_Output::ckNew(2,0,domains[0],numElements,f[6],f[7]);
  outDone.reset( (DoneMsg*) CkWaitFuture(f[7]) );

  ckout << "And last but not least,\n"
	<< "we set domain 1 to be the right-hand-side of domain 2."
	<< endl;
  CProxy_SetToRhs::ckNew(domains[0],domains[1],f[8]);
  ckout << "Wait for it to finish..." << endl;
  outDone.reset( (DoneMsg*) CkWaitFuture(f[8]) );
  ckout << "And let's output it." << endl;
  CProxy_Output::ckNew(3,0,domains[0],numElements,f[9],f[10]);
  outDone.reset( (DoneMsg*) CkWaitFuture(f[10]) );
  ckout << "And now let's get the right-hand side of the 4th-index\n"
	<< "of domain 1"
	<< endl;
  domains[1][4].getRhs(f[11]);
  std::unique_ptr<GridMsg> mRhs( (GridMsg*) CkWaitFuture(f[11]) );
  ckout << "Received!" << endl;
  ckout << "Let's see what it looks like." << endl;
  Grid testGrid;
  mRhs -> fillGrid(testGrid);
  ckout << "Filled grid. Let's see what it looks like!" << endl;
  ss.str("");
  ss << testGrid;
  ckout << "rhs:\n"
	<< ss.str().c_str()
	<< endl;
  ckout << "Now we clean up." << endl;
  for (int i = 0; i < numFutures; ++i) CkReleaseFuture(f[i]);
  ckout << "And we're done!" << endl;
}

#include "wavetoy.def.h"
